<?php 

use PHPUnit\Framework\TestCase;

class CollectionTest extends TestCase 
{

	

	public function test_empy_instantiated_collection_returns_no_items()
	{
		$collection = new \App\Support\Collection;
	
		$this->assertEmpty($collection->get());
	}

	public function test_count_is_correct_for_items_passed_in()
	{
		$collection = new \App\Support\Collection([
			'one', 'two', 'three'
		]);

		$this->AssertEquals(3, $collection->count());
	}
	
	public function test_items_returned_match_items_passed_in()
	{
		$collection = new \App\Support\Collection([
			'one', 'two'
		]);
		$this->AssertCount(2, $collection->get());
		$this->AssertEquals($collection->get()[0], 'one');
		$this->AssertEquals($collection->get()[1], 'two');


	}

	public function test_collection_is_instance_of_iterator_aggregate()
	{
		$collection = new \App\Support\Collection([
			'one', 'two'
		]);

		$this->assertInstanceOf(IteratorAggregate::class, $collection);

	}

	public function test_collection_can_be_iterated()
	{
		$collection = new \App\Support\Collection([
			'one', 'two', 'three'
		]);

		$items = [];

		foreach ($collection as $item) {
			$items[] = $item;
		}

		$this->assertCount(3, $items);
		$this->assertInstanceOf(ArrayIterator::class, $collection->getIterator());
	}

	public function test_collection_can_be_merged_with_another_collection()
	{
		$collection1 = new \App\Support\Collection(['one', 'two']);
		$collection2 = new \App\Support\Collection(['three', 'four', 'five']);
		
		$collection1->merge($collection2);
		
		$this->assertCount(5, $collection1->get());
		$this->assertEquals(5, $collection1->count());
	}

	public function test_can_to_existing_collection()
	{
		$collection = new \App\Support\Collection(['one', 'two']);
		$collection->add(['three']);

		$this->assertCount(3, $collection->get());
		$this->assertEquals(3, $collection->count());

	}

	public function test_return_json_encoded_items()
	{
		$collection = new \App\Support\Collection(['one', 'two']);
		$collections = new \App\Support\Collection([
			['username' => 'sadiq'],
			['username' => 'noor']
		]);
		//print_r($collection->json());exit;
		$this->assertEquals('["one","two"]', $collection->json());
		$this->assertEquals('[{"username":"sadiq"},{"username":"noor"}]', $collections->json());
	}

}
