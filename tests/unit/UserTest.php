<?php

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase {

	public function testThatWeCanGetTheFirstName()
	{
		$user = new App\Models\User;
		
		$user->setFirstName('sadiq');
		
		$this->assertEquals($user->getFirstName(), 'sadiq');
	}

	public function testThatWeCanGetTheFLastName()
	{
		$user = new App\Models\User;
		
		$user->setLastName('noor');
		
		$this->assertEquals($user->getLastName(), 'noor');
	}

	public function testFullNameisReturned()
	{
		$user = new App\Models\User;
		
		$user->setLastName('noor');
		$user->setFirstName('sadiq');

		
		$this->assertEquals($user->getFullName(), 'sadiq noor');
	}

	public function testFullNameisAreTrimed()
	{
		$user = new App\Models\User;
		
		$user->setFirstName('sadiq  ');
		$user->setLastName('   noor');

		
		$this->assertEquals($user->getFirstName(), 'sadiq');
		$this->assertEquals($user->getLastName(), 'noor');

	}


	public function testEmailAddressCanBeset()
	{
		$user = new App\Models\User;

		$user->setEmail('sadiq.pk33@gmail.com');

		$this->assertEquals($user->getEmail(), 'sadiq.pk33@gmail.com');
	}

	
	public function testEmailVariablesContainCorrectValues()
	{
		$user = new App\Models\User;
		
		$user->setFirstName('sadiq');
		$user->setLastName('noor');
		$user->setemail('sadiq.pk33@gmail.com');

		$emailVariables = $user->getEmailVariables();

		
		$this->assertArrayHasKey('full_name', $emailVariables);
		$this->assertArrayHasKey('email', $emailVariables);

		$this->assertEquals($emailVariables['full_name'], 'sadiq noor');
		$this->assertEquals($emailVariables['email'], 'sadiq.pk33@gmail.com');


	}

}
