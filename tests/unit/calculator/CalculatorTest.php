<?php

use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{

    function test_can_set_single_operation()
    {
        $addition = new \App\Calculator\Addition;
        $addition->setOperands([5, 10]);
     
       // $this->assertEquals(15, $addition->calculate());

        $calculator = new \App\Calculator\Calculator;
        $calculator->setOperation($addition);

        $this->assertCount(1, $calculator->getOperations());
    }

    public function test_can_set_multiple_operations()
    {
        $addition = new \App\Calculator\Addition;
        $addition->setOperands([5, 10]);

        $addition2 = new \App\Calculator\Addition;
        $addition2->setOperands([2, 7]);  
        
        $calculator = new \App\Calculator\Calculator;
        $calculator->setOperations([$addition, $addition2]);
        
        $this->assertCount(2, $calculator->getOperations());

    }

    public function test_operation_are_ignore_if_not_instance_of_operation_interface()
    {
        $addition = new \App\Calculator\Addition;
        $addition->setOperands([5, 10]);

        $calculator = new \App\Calculator\Calculator;
        $calculator->setOperations([$addition, 'dogs', 'cat']);

        $this->assertCount(1, $calculator->getOperations());

    }

    public function test_can_calculate_result()
    {
        $addition = new \App\Calculator\Addition;
        $addition->setOperands([5, 10]);

        $calculator = new \App\Calculator\Calculator;
        $calculator->setOperation($addition);

        $this->assertEquals(15, $calculator->calculate());


    }

    public function test_calculate_method_returns_multiple_results()
    {
        $addition = new \App\Calculator\Addition;
        $addition->setOperands([5, 10]);


        $division = new \App\Calculator\Division;
        $division->setOperands([100, 2]);

        $calculator = new \App\Calculator\Calculator;
        $calculator->setOperations([$addition, $division]);
// print_r($calculator->calculate()); exit;
       // $this->assertInternalType('array', $calculator->calculate());
        $this->assertEquals(15, $calculator->calculate()[0]);
        $this->assertEquals(50, $calculator->calculate()[1]);


       // $this->assertEquals(15, $calculator->calculate());


    }

}