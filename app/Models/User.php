<?php

namespace App\Models;

class User
{
    public $first_name;
	public $last_name;
	public $email;

	public function __construct(){
		//print_r('sdfsdf'); exit;
	}
	public function setFirstName($firstName){
		$this->first_name =  trim( $firstName);
	}

	public function getFirstName(){
		return $this->first_name;
	}


	/**
	 * Get the value of last_name
	 */ 
	public function getLastName()
	{
		return $this->last_name;
	}

	/**
	 * Set the value of last_name
	 *
	 * @return  self
	 */ 
	public function setLastName($lastName)
	{
		$this->last_name = trim($lastName);

		return $this;
	}

	public function getFullName()
	{
		return $this->first_name. " " . $this->last_name;
	}

	/**
	 * Get the value of email
	 */ 
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * Set the value of email
	 *
	 * @return  self
	 */ 
	public function setEmail($email)
	{
		$this->email = $email;

		return $this;
	}

	public function getEmailVariables()
	{
		return [
			'full_name' => $this->getFullName(),
			'email' => $this->getEmail()
		];
	}
}

?>