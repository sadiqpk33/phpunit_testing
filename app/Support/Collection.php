<?php

namespace App\Support;
use ArrayIterator;
use IteratorAggregate;

class Collection implements IteratorAggregate
{

    public $items = [];

    public function __construct(array $items = [])
    {
        $this->items = $items;
    }
    public function get()
    {
        return $this->items;
    }

    public function count(): int
    {
        return count($this->items);
    }

    public function getIterator(): \Traversable
    {
        return new ArrayIterator($this->items);
    }
    /**
     * 
     *
     * @param Collection $collection
     * @return array
     */
    public function merge(Collection $collection)
    {
        return $this->add($collection->get());
        //return new Collection(array_merge($this->get(), $collection->get()));
    }
    
    public function add($collection)
    {
        $this->items =  array_merge($this->items, $collection);
    }

    public function json()
    {
        return json_encode($this->items);
    }

}